<?php

namespace Drupal\entity_form_hijack;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\GeneratedUrl;
use Drupal\Core\Routing\LocalRedirectResponse;
use Drupal\Core\Routing\RedirectDestinationInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\CacheableTypes\CacheableBool;
use Drupal\route_override\Interfaces\RouteOverrideControllerBase;
use Drupal\route_override\Traits\OverrideEntityFormFromBundleConfigTrait;
use Drupal\route_override\Traits\OverrideEntityFormTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Route;

/**
 * Implements class for HijackEntityFormControllerOverride.
 */
final class HijackEntityFormControllerOverride extends RouteOverrideControllerBase {

  use StringTranslationTrait;

  use OverrideEntityFormTrait;

  use OverrideEntityFormFromBundleConfigTrait;

  /**
   * The entity manager service.
   *
   * @var Drupal\Core\Entity\EntityTypeInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The redirect destination service.
   *
   * @var \Drupal\Core\Routing\RedirectDestinationInterface
   */
  protected RedirectDestinationInterface $redirectDestination;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected RequestStack $requestStack;

  /**
   * Constructs a new HijackEntityFormControllerOverride object.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, RedirectDestinationInterface $redirectDestination, RequestStack $requestStack) {
    $this->entityTypeManager = $entityTypeManager;
    $this->redirectDestination = $redirectDestination;
    $this->requestStack = $requestStack;
  }

  /**
   * Implements appliesToRouteOfEntityFormOfBundle().
   */
  protected function appliesToRouteOfEntityFormOfBundle(ConfigEntityInterface $bundleConfig, EntityTypeInterface $entityType, Route $route): CacheableBool {
    $isDeleteForm = $this->isEntityDeleteFormRoute($route, $entityType->id());
    $isCreateForm = $this->isEntityCreateFormRoute($route, $entityType->id());
    if ($isDeleteForm->value()) {
      // A delete-form, so not our business and never will be: No additional
      // cacheability.
      return CacheableBool::create(FALSE, $isDeleteForm);
    }
    else {
      $applies = (bool) $this->getRedirectPathForBundleConfig($bundleConfig, $isCreateForm->value());
      return CacheableBool::create($applies, $bundleConfig);
    }
  }

  /**
   * Implements appliesToRouteMatchOfEntityFormOfBundle().
   */
  protected function appliesToRouteMatchOfEntityFormOfBundle(EntityInterface $entity, ConfigEntityInterface $bundleConfig, RouteMatchInterface $route_match, Request $request): CacheableBool {
    $applies = (bool) $this->getRedirectPathForEntity($entity);
    return CacheableBool::create($applies, $bundleConfig);
  }

  /**
   * Implements boolAccess().
   */
  protected function boolAccess(RouteMatchInterface $route_match, AccountInterface $account, Request $request = NULL): CacheableBool {
    $entity = $this->extractEntityFromRouteMatchOfEntityForm($route_match);
    $redirectPath = $this->getRedirectPathForEntity($entity);
    // This is called only if ::applies() returns true.
    assert(!empty($redirectPath));

    // Add destination to redirect url, but only if not explicitly given.
    $url = Url::fromUserInput($redirectPath);

    // Override route has access if and only if
    $accessResult = $url->access(NULL, TRUE);
    return CacheableBool::fromAccessResult($accessResult);
  }

  public function build(RouteMatchInterface $routeMatch, Request $request) {
    $entity = $this->extractEntityFromRouteMatchOfEntityForm($routeMatch);
    $redirectPath = $this->getRedirectPathForEntity($entity);
    // This is called only if ::appliesToRouteMatchOfEntityFormOfBundle
    assert(!empty($redirectPath));

    if (!\is_null($entity->id())) {
      $redirectPath = strtr($redirectPath, ['{id}' => $entity->id()]);
    }

    // Add destination to redirect url, but only if not explicitly given.
    $url = Url::fromUserInput($redirectPath);
    $redirectQuery = $url->getOption('query') ?? [];
    // Don't use RedirectDestination, as this returns current url if no query.
    $urlDestination = $request->query->get('destination');
    if (!isset($redirectQuery['destination']) && $urlDestination) {
      $redirectQuery += ['destination' => $urlDestination];
      $url->setOption('query', $redirectQuery);
      $generatedUrl = $url->toString(TRUE);
      assert($generatedUrl instanceof GeneratedUrl);
      $redirectPath = $generatedUrl->getGeneratedUrl();
    }

    // For CacheableRedirectResponse, destination trumps.
    // Note, not RedirectDestination, but $request->query->get('destination').
    // A SecuredRedirectResponse does not help, as query destination is 'safe'.
    // @see \Drupal\Core\EventSubscriber\RedirectResponseSubscriber::checkRedirectUrl
    // So sorry kittens, delete $request->query->get('destination').
    // @todo Is there any way to fix destination handling upstream in a BC way?
    //   https://www.drupal.org/project/drupal/issues/3269760
    $this->requestStack->getCurrentRequest()->query->remove('destination');

    $response = new LocalRedirectResponse($redirectPath);
    if (isset($generatedUrl)) {
      $response->addCacheableDependency($generatedUrl);
    }
    return $response;
  }

  /**
   * Implements getRedirectPathForEntity().
   */
  private function getRedirectPathForEntity(EntityInterface $entity): ?string {
    if ($bundleConfig = $this->getMaybeBundleConfigOfEntity($entity)) {
      $isCreateForm = $entity->isNew();
      // @noinspection PhpUnnecessaryLocalVariableInspection
      $result = $this->getRedirectPathForBundleConfig($bundleConfig, $isCreateForm);
      return $result;
    }
    return NULL;
  }

  /**
   * Implements getRedirectPathForBundleConfig().
   */
  private function getRedirectPathForBundleConfig(ConfigEntityInterface $bundleConfig, bool $isCreateForm) {
    // @noinspection PhpUnnecessaryLocalVariableInspection
    $result = $isCreateForm ?
      $bundleConfig->getThirdPartySetting('entity_form_hijack', 'redirect_create', NULL) :
      $bundleConfig->getThirdPartySetting('entity_form_hijack', 'redirect_edit', NULL);
    return $result;
  }

}
